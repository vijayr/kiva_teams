<?php

    error_reporting(E_ALL);
    ini_set('display_errors', TRUE);

    define("TEAMS_API", "https://api.kivaws.org/v1/teams/search.json");
    define("TEAMS_DATA", "../data/teams.csv");

    /*
        Just being paranoid, don't want to hit Kiva servers too fast. increase/decrease this number (in seconds) as you see fit
    */
    define("SLEEP_FOR", 3);

    require ("teams.class.php");

    $teams = new teams();
    $teams->process();

