<?php

    class teams {
        
        private $teams_file;

        public function __construct() {
            $this->teams_file = fopen(TEAMS_DATA, "w");
        }

        private function print_headers() {
            // headers for the CSV file, keep it the same as the array keys from the API 
            $teams_file_headers = ['id', 'shortname', 'name', 'category', 'image_id', 'image_template_id',
                            'whereabouts', 'loan_because', 'description',
                            'website_url', 'team_since', 'membership_type', 'member_count',
                            'loan_count', 'loaned_amount'];

            fputcsv($this->teams_file, $teams_file_headers);
        }

        /*
            function to call the teams API, with some time gap between successive calls.
            parse the result from the API and write the data to a CSV file
        */
        public function process() {
            $this->print_headers();

            $current_page = 1;
            $total_pages = 1;              

            while($current_page <= $total_pages) {
                $current_url = TEAMS_API . "?page=" . $current_page;
                $data = $this->get_curl_data($current_url);

                if (isset($data['paging']) && is_array($data['paging']) && count($data['paging']) > 0) {
                    $paging = $data['paging'];
                    $total_pages = (isset($paging['pages'])) ? $paging['pages'] : $total_pages;
                }

                if (isset($data['teams']) && is_array($data['teams']) && count($data['teams']) > 0) {
                    $teams = $data['teams'];
                    $this->print($teams);
                }        

                $current_page ++;
                sleep(SLEEP_FOR);
            }

            fclose($this->teams_file);         
        }

        // use curl to get teams data and return teams array
        private function get_curl_data($url) {
            $curl = curl_init(); 
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            $json_output = curl_exec($curl);
            if(curl_error($curl)) {
                die("Error during curl call : " . curl_error($curl));
            }
            curl_close($curl);

            $data = json_decode($json_output, TRUE);
            return $data;
        }

        function print($teams) {
            if (is_array($teams) && count($teams) > 0) {
                foreach ($teams as $t) {
                    $team = array();

                    $team[] = $team_id = isset($t['id']) ? $t['id'] : "";
                    $team[] = isset($t['shortname']) ? $t['shortname'] : "";
                    $team[] = isset($t['name']) ? $t['name'] : "";
                    $team[] = isset($t['category']) ? $t['category'] : "";
                    $team[] = isset($t['image']['id']) ? $t['image']['id'] : "";
                    $team[] = isset($t['image']['template_id']) ? $t['image']['template_id'] : "";
                    $team[] = isset($t['whereabouts']) ? $t['whereabouts'] : "";
                    $team[] = isset($t['loan_because']) ? $t['loan_because'] : "";
                    $team[] = isset($t['description']) ? $t['description'] : "";
                    $team[] = isset($t['website_url']) ? $t['website_url'] : "";
                    $team[] = isset($t['team_since']) ? $t['team_since'] : "";
                    $team[] = isset($t['membership_type']) ? $t['membership_type'] : "";
                    $team[] = isset($t['member_count']) ? $t['member_count'] : "";
                    $team[] = isset($t['loan_count']) ? $t['loan_count'] : "";
                    $team[] = isset($t['loaned_amount']) ? $t['loaned_amount'] : "";

                    fputcsv($this->teams_file, $team);
                }
            }
        }
    }
